function factorial(number) {
  if (typeof number !== "number") return undefined;
  if (number < 0) return undefined;
  if (number === 0) return 1;
  if (number === 1) return 1;
  return number * factorial(number - 1);
}

function div_check(number) {
  if (typeof number !== "number") return false;

  if (number % 5 === 0 || number % 7 === 0) {
    return true;
  } else {
    return false;
  }
}

module.exports = {
  factorial: factorial,
  div_check: div_check,
};
