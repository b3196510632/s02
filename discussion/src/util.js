function factorial(number) {
  if (typeof number !== "number") return undefined; // is to check if the value is number
  if (number < 0) return undefined; // is to check if the value should be more than 0
  if (number === 0) return 1; // is to check if the value is 0 return 1
  if (number === 1) return 1; // is to check if the value is 1 return 1
  return number * factorial(number - 1);
}

module.exports = {
  factorial: factorial,
};
